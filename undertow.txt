# 配置样例文件 undertow.txt 使用说明：
#
# 1：系统默认在 class path 根目录下先加载 undertow.txt 再加载 undertow-pro.txt
#    进行配置，当上述两个配置文件不存在时不抛异常并使用默认值配置
#
# 2：所有配置可以省略，省略时使用默认配置
#
# 3：开发阶段 undertow.devMode 配置为 true 才支持热加载
#
# 4：该文件列出了绝大多数可配置项，更多不常用配置可以查看 UndertowConfig 源码中的配置常量定义
#
# 5：当配置项不足以满足需求时，通过查看官方文档进行更多灵活配置：
#    https://jfinal.com/doc/1-4
#


# true 值支持热加载
undertow.devMode=true

# 避免项目中的 .class 打成 jar 包以后，同时在使用 devMode 时报的异常
# 只要 underto.devMode 设置为 false，或者不打包就不会有异常
# 添加此配置以后则无需关心上面这类事情，多个前缀用逗号分隔开
undertow.hotSwapClassPrefix=com.jfinal.club.


undertow.port=80
undertow.host=0.0.0.0
undertow.contextPath=/

# 配置 css、js、图片等等 web 资源路径
# "src/main/webapp" 表示从普通路径中加载资源， "classpath:static" 表示从 classpath 与 jar 包中的 static 路径下加载资源
# undertow.resourcePath=src/main/webapp, classpath:static

# 配置是否打开资源文件变动监听器，配置为 false 可解决项目资源文件过多所导致的启动过慢问题
# undertow.allowResourceChangeListeners = false

# IO 线程数，一般建议使用默认值
# undertow.ioThreads=

# worker 线程数，一般建议使用默认值。高性能场景下可配置为：cores / (1 - 阻塞系数)
# 其中 cores 为 cpu 核心数量，阻塞系数 = 阻塞时间 / (阻塞时间 + 使用CPU的时间)
# undertow.workerThreads=


# gzip 压缩开关
undertow.gzip.enable=false
# 配置压缩级别，默认值 -1。 可配置 1 到 9。 1 拥有最快压缩速度，9 拥有最高压缩率
undertow.gzip.level=-1
# 触发压缩的最小内容长度
undertow.gzip.minLength=1024

# session 过期时间，注意单位是秒
# undertow.session.timeout=1800
# 热加载保持 session 值，避免依赖于 session 的登录型项目反复登录，默认值为 true。仅用于 devMode，生产环境无影响
# undertow.session.hotSwap=true

# SSL 配置 ----------------------------------------------------------------------------------
# 生产环境从阿里云下载 tomcat 类型的密钥库。以下两行命令生成密钥库仅用于测试：
# keytool -genkeypair -validity 3650 -alias club -keyalg RSA -keystore club.jks
# keytool -importkeystore -srckeystore club.jks -destkeystore club.pfx -deststoretype PKCS12
# 生成过程中提示输入 "名字与姓氏" 时输入 localhost
#
# 是否开启 ssl
# undertow.ssl.enable=false
# ssl 监听端口号，部署环境设置为 443
# undertow.ssl.port=443
# 密钥库类型，建议使用 PKCS12
# undertow.ssl.keyStoreType=PKCS12
# 密钥库文件
# undertow.ssl.keyStore=club.pfx
# 密钥库密码
# undertow.ssl.keyStorePassword=111111
# 别名配置，一般不使用
# undertow.ssl.keyAlias=club

# 配置 ciphers，多个配置用冒号分隔，详情见：https://jfinal.com/share/2323
# undertow.ssl.ciphers = TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
# 配置 enabledProtocols，多个配置用空格分隔
# undertow.ssl.enabledProtocols = TLSv1 TLSv1.1 TLSv1.2

# ssl 开启时，是否开启 http2。chrome 开发者工具 -> Network -> Protocol 这一栏出现 h2 时表明 http2 被开启
# undertow.http2.enable=true

# ssl 开启时，http 请求是否重定向到 https
# undertow.http.toHttps=false
# ssl 开启时，http 请求跳转到 https 使用的状态码，默认值 302
# undertow.http.toHttpsStatusCode=302
# ssl 开启时，是否关闭 http
# undertow.http.disable=false


# 在 HTTP response header 中显示服务名，配置为 disable 时表示不启用
# undertow.serverName=disable



